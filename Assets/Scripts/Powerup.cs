﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.Altega
{
	public class Powerup : MonoBehaviour
	{
		const float kFallSpeed = 3.0f;

		private bool _done;

		protected void FixedUpdate()
		{
			float newY = transform.position.y - kFallSpeed * Time.fixedDeltaTime;
			transform.position = new Vector3 (transform.position.x, newY, 0f);
		}

		void OnCollisionEnter2D(Collision2D collision)
		{
			GameManager.Instance.splitBalls ();
			Destroy (gameObject);
		}
	}
}
