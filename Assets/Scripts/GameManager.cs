﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
	public GameObject paddle;

	public GameObject ballSpawn;

	public GameObject ballPrefab;

	public GameObject powerupPrefab;

	public List<GameObject> levels;

	public Text levelText;

	public Text startText;

	public Text livesText;

	public Text winText;

	public GameObject gameOverOverlay;

	private const string kPaddleAxis = "Horizontal";

	private const float kPaddleSpeed = 8.0f;

	private const float kPowerupProb = 0.3f;

	private const int kMaxBalls = 8;

	private const int kMaxLives = 3;

	private float _paddleXBound;

	private GameObject _curLevel;

	private int _curLevelNum;

	private bool _waitingForShoot = false;

	private bool _gameOver = false;

	private List<Ball> _balls = new List<Ball> ();

	private int _numLives;

	private int _bricksRemaining;

	protected void Start ()
	{
		SpriteRenderer paddleRenderer = paddle.GetComponent<SpriteRenderer> ();
		_paddleXBound = Camera.main.ViewportToWorldPoint (Vector3.one).x - paddleRenderer.bounds.extents.x;
		_numLives = kMaxLives;
		_loadLevel (0);
	}

	protected void Update ()
	{
		if (Input.GetButtonDown ("Fire1")) {
			if (_gameOver) {
				SceneManager.LoadScene (0);
			} else if(_waitingForShoot)  {
				_shootBall (ballSpawn.transform.position);
			}
		}
	}

	protected void FixedUpdate ()
	{
		float newX = paddle.transform.position.x + Input.GetAxis (kPaddleAxis) * kPaddleSpeed * Time.fixedDeltaTime;
		newX = Mathf.Clamp (newX, -_paddleXBound, _paddleXBound);
		paddle.transform.position = new Vector3 (newX, paddle.transform.position.y, 0);
	}

	public void splitBalls() {
		if (_balls.Count >= kMaxBalls) {
			return;
		}
		int curBallCount = _balls.Count;

		for (int i = 0; i < curBallCount; i++) {
			Ball ball = _balls [i];
			Ball splitBall = _spawnBall (ball.transform.position);
			splitBall.StartMove ();
			if (_balls.Count >= kMaxBalls) {
				return;
			}
		}
	}

	public void removeBall(GameObject ballObj) {
		Ball ball = ballObj.GetComponent<Ball>();
		if (ball == null) {
			return;
		}

		_balls.Remove (ball);
		Destroy (ballObj);
		if (_balls.Count == 0) {
			_numLives--;
			_removePowerups();

			if (_numLives > 0) {
				_placeStartBall ();
			} else {
				_doGameOver ();
			}
		}
	}

	public void removeBrick(GameObject brick)
	{
		_bricksRemaining--;
		if (_bricksRemaining == 0) {
			//EditorApplication.isPaused = true;
			_doLevelWin ();
		} else if (_balls.Count < kMaxBalls && Random.value <= kPowerupProb) {
			_spawnPowerup (brick.transform.position);
		}
	}

	private void _doGameOver()
	{
		_removePowerups ();
		_gameOver = true;
		gameOverOverlay.SetActive(true);
	}

	private void _doLevelWin()
	{
		foreach (Ball ball in _balls) {
			Destroy (ball.gameObject);
		}
		_balls.Clear ();
		_removePowerups ();
		if (_curLevelNum == levels.Count - 1) {
			_curLevel.SetActive (false);
			winText.gameObject.SetActive (true);
		} else {
			_loadLevel (_curLevelNum + 1);
		}
	}

	private void _shootBall (Vector3 pos)
	{
		if (!_waitingForShoot) {
			Debug.LogError ("Must be waiting to shoot to shoot");
		}

		if (_balls.Count != 1) {
			Debug.LogError ("Trying to shoot when ball count is not 1");
		}

		_balls [0].StartMove ();
		levelText.gameObject.SetActive (false);
		startText.gameObject.SetActive (false);
		livesText.gameObject.SetActive (false);
		_waitingForShoot = false;
	}

	private void _loadLevel (int levelNum)
	{
		if (levelNum < 0 || levelNum >= levels.Count) {
			Debug.LogError ("Invalid Level: " + levelNum);
		}

		if (_curLevel != null) {
			_curLevel.SetActive (false);
		}

		_curLevel = levels [levelNum];
		_curLevelNum = levelNum;
		_curLevel.SetActive (true);

		Brick[] levelBricks = _curLevel.GetComponentsInChildren<Brick> ();
		_bricksRemaining = 0;
		foreach (Brick brick in levelBricks) {
			if (brick.enabled) {
				_bricksRemaining++;
			}
		}
		levelText.gameObject.SetActive (true);

		levelText.text = "Level " + (levelNum + 1);
		_placeStartBall ();
	}

	private Ball _spawnBall (Vector3 pos)
	{
		GameObject ballObj = Instantiate (ballPrefab, pos, Quaternion.identity);
		Ball ball = ballObj.GetComponent<Ball> ();
		_balls.Add (ball);

		return ball;
	}

	private void _spawnPowerup(Vector3 pos)
	{
		if (_waitingForShoot) {
			return;
		}
		Instantiate (powerupPrefab, pos, Quaternion.identity);
	}

	private void _removePowerups()
	{
		foreach (GameObject powerup in GameObject.FindGameObjectsWithTag("powerup")) {
			Destroy (powerup);
		}
	}

	private void _placeStartBall ()
	{
		if (_balls.Count != 0) {
			Debug.LogError ("Trying to start ball while some are still in play");
			return;
		}

		_spawnBall (ballSpawn.transform.position);

		startText.gameObject.SetActive (true);
		livesText.text = "Lives: " + _numLives;
		livesText.gameObject.SetActive (true);

		_waitingForShoot = true;
	}
}

