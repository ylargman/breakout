﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
	private const float kDestroyDelay = 0.3f;

	private const string kBrickSpriteBase = "brick";

	private const string kMetalSpriteName = "brickmetal";

	private const string kBreakSpriteName = "bricksplosion";

	private int _health;

	private SpriteRenderer _renderer;

	protected void Awake()
	{
		_renderer = GetComponent<SpriteRenderer> ();
		string spriteName = _renderer.sprite.name;
		if (spriteName == kMetalSpriteName) {
			enabled = false;
			return;
		} else {
			_health = (int)System.Char.GetNumericValue (spriteName [spriteName.Length - 1]);
		}
	}

	void OnCollisionExit2D(Collision2D collision)
	{
		if (!enabled) {
			return;
		}
		_health--;
		if (_health <= 0) {
			enabled = false;
			_setSprite (kBreakSpriteName);
			GetComponent<Collider2D> ().enabled = false;

			Invoke ("_cleanup", kDestroyDelay);
		} else {
			_setSprite (kBrickSpriteBase + _health);
		}
	}

	private void _cleanup()
	{
		GameManager.Instance.removeBrick (gameObject);
		Destroy (gameObject);
	}

	private void _setSprite(string fileName)
	{
		_renderer.sprite = Resources.Load<Sprite> (fileName);
	}
}
