﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T _instance;

	public static T Instance { get { return _instance;  }}

	void Awake()
	{
		if (_instance == null) {
			_instance = this as T;
		}

		else if (_instance != this) {
			Destroy (gameObject);	
		}
	}
}