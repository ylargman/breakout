﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
	public Sprite defaultSprite;

	public Sprite hitSprite;

	private const float kHitDuration = 0.25f;

	private const float kMinAxisVelocity = 0.7f;

	private const float kInitImpulseMag = 0.8f;

	private SpriteRenderer _renderer;

	private Rigidbody2D _body;

	private float _curHitTime = 0.0f;

	private float _velMagnitude = 0.0f;

	protected void Awake ()
	{
		_renderer = GetComponent<SpriteRenderer> ();
		_renderer.sprite = defaultSprite;
		_body = GetComponent<Rigidbody2D> ();
	}
		
	// Update is called once per frame
	protected void Update ()
	{
		if (_curHitTime > 0.0f) {
			_curHitTime -= Time.deltaTime;
			if (_curHitTime <= 0.0f) {
				_curHitTime = 0.0f;
				_renderer.sprite = defaultSprite;
			}
		}
	}

	public void StartMove()
	{
		float initX = Random.Range (0.2f, 0.6f);
		if (Random.value > 0.5f) {
			initX = -initX;
		}
		_body.AddForce (new Vector3 (initX, 1.0f, 0f).normalized * kInitImpulseMag,
			ForceMode2D.Impulse);
	}

	void OnCollisionExit2D(Collision2D collision)
	{
		_curHitTime = kHitDuration;
		_renderer.sprite = hitSprite;

		if (_velMagnitude == 0.0f) {
			_velMagnitude = _body.velocity.magnitude;
		}

		float newX = _body.velocity.x;
		float newY = _body.velocity.y;

		if (Mathf.Abs(newY) < kMinAxisVelocity) {
			float direction;
			if (newY != 0.0f) {
				direction = Mathf.Sign (newY);
			} else {
				direction = -Mathf.Sign(transform.position.y);
			}
			newY = kMinAxisVelocity * direction;
		}

		if (Mathf.Abs(newX) < kMinAxisVelocity) {
			float direction;
			if (newX != 0.0f) {
				direction = Mathf.Sign (newX);
			} else {
				direction = -Mathf.Sign (transform.position.x);
			}
			newX = kMinAxisVelocity * direction;
		}

		Vector2 newVel = Vector2.ClampMagnitude (new Vector2(newX, newY), _velMagnitude);
		_body.velocity = newVel;
	}
}
